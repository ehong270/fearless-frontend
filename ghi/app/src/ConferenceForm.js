import React from 'react';

class ConferenceForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            name: '',
            startDate: '',
            endDate: '',
            description: '',
            maximumPresentations: '',
            maximumAttendees: '',
            locations: []
        }
        this.handleNameChange = this.handleNameChange.bind(this)
        this.handleStartDateChange = this.handleStartDateChange.bind(this)
        this.handleEndDateChange = this.handleEndDateChange.bind(this)
        this.handleDescriptionChange = this.handleDescriptionChange.bind(this)
        this.handleMaximumPresentationsChange = this.handleMaximumPresentationsChange.bind(this)
        this.handleMaximumAttendeesChange = this.handleMaximumAttendeesChange.bind(this)
        this.handleLocationChange = this.handleLocationChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)

    }
    handleNameChange(event) {
        const value = event.target.value;
        this.setState({name: value})
      }
      handleStartDateChange(event) {
        const value = event.target.value
        this.setState({startDate: value})
      }
      handleEndDateChange(event) {
        const value = event.target.value
        this.setState({endDate: value})
      }
      handleDescriptionChange(event) {
        const value = event.target.value
        this.setState({description: value})
      }
      handleMaximumPresentationsChange(event) {
        const value = event.target.value;
        this.setState({maximumPresentations: value})
      }
      handleMaximumAttendeesChange(event) {
        const value = event.target.value
        this.setState({maximumAttendees: value})
      }
      handleLocationChange(event) {
        const value = event.target.value
        this.setState({location: value})
      }
      async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        data.starts = data.startDate
        data.ends = data.endDate
        data.max_presentations = data.maximumPresentations;
        data.max_attendees = data.maximumAttendees;
        delete data.startDate
        delete data.endDate
        delete data.maximumPresentations;
        delete data.maximumAttendees;
        delete data.locations;
        console.log(data);
        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };

        const response = await fetch(conferenceUrl, fetchConfig);

    if (response.ok) {
        const newConference = await response.json();
        console.log(newConference);

        const cleared = {
            name: '',
            startDate: '',
            endDate: '',
            description: '',
            maximumPresentations: '',
            maximumAttendees: '',
            location: ''
        };
        this.setState(cleared);
      }
  }


        async componentDidMount() {
        const url = 'http://localhost:8000/api/locations/';

        const response = await fetch(url);

        if (response.ok) {
          const data = await response.json();
          this.setState({locations: data.locations});


        }
      }


render() {
    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new conference</h1>
            <form onSubmit={this.handleSubmit} id="create-conference-form">
              <div className="form-floating mb-3">
                <input value={this.state.name} onChange={this.handleNameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control"/>
                <label htmlFor="name">Name</label>
                </div>
                <div className="form-floating mb-3">
                <input value={this.state.startDate} onChange={this.handleStartDateChange} placeholder="Start date" name="starts" type="date" id="starts" className="form-control"/>
                <label htmlFor="starts">Start date</label>
              </div>
              <div className="form-floating mb-3">
                <input value={this.state.endDate} onChange={this.handleEndDateChange} placeholder="End date" name="ends" type="date" id="ends" className="form-control"/>
                <label htmlFor="ends">End date</label>
              </div>
              <div className="mb-3">
                <label htmlFor="exampleFormControlTextarea1" className="form-label"></label>
                <textarea value={this.state.description} onChange={this.handleDescriptionChange} className="form-control" placeholder="Description" id="exampleFormControlTextarea1" rows="3"></textarea>
              </div>
              <div className="form-floating mb-3">
                <input value={this.maximumPresentation} onChange={this.handleMaximumPresentationsChange} placeholder="Maximum presentations" name="max_presentations" required type="number" id="max_presentations" className="form-control"/>
                <label htmlFor="room_count">Maximum presentations</label>
              </div>
              <div className="form-floating mb-3">
                <input value={this.maximumAttendees} onChange={this.handleMaximumAttendeesChange} placeholder="Maximum attendees" name="max_attendees" required type="number" id="max_attendees" className="form-control"/>
                <label htmlFor="room_count">Maximum attendees</label>
              </div>
              <div className="mb-3">
                <select onChange={this.handleLocationChange} required id="location" name="location" className="form-select">
                  <option value="">Choose a location</option>
                  {this.state.locations.map(location => {
                    return (
                        <option key={location.id} value={location.id}>
                        {location.name}
                        </option>
                    )
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    )
}
}

export default ConferenceForm
